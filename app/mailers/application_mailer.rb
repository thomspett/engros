class ApplicationMailer < ActionMailer::Base
  default from: 'kundeservice@garnengros.no'
  layout 'mailer'
end
