class CreateJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :yarns, :designs do |t|
      t.index [:yarn_id, :design_id]
      t.index [:design_id, :yarn_id]
      t.index [:yarn_id, :design_id], name: 'index_yarns_designs', unique: true
    end
  end
end
