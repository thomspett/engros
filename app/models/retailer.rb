class Retailer < ApplicationRecord
	has_and_belongs_to_many :yarns

	def has_some_yarns_for_design(design)
		# Is the intersection non-empty?
		(design.yarns.length - (design.yarns - yarns).length) > 0
	end

	def has_all_yarns_for_design(design)
		#Is the design.yarns a subset of the retailer.yarns?
		(design.yarns - yarns).empty?
	end
end
