class Yarn < ApplicationRecord
  has_and_belongs_to_many :designs
  has_and_belongs_to_many :retailers
end
