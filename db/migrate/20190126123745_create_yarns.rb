class CreateYarns < ActiveRecord::Migration[5.2]
  def change
    create_table :yarns do |t|
      t.string :name
      t.integer :length
      t.integer :weight

      t.timestamps
    end
  end
end
