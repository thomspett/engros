class Lead < ApplicationRecord
	validates :company_name, :contact, :email, presence: true
	validates :email, uniqueness: { case_sensitive: false }

	before_save :downcase_email

	def downcase_email
		self.email.downcase!
	end
end
