class AddAddressToRetailers < ActiveRecord::Migration[5.0]
  def change
    add_column :retailers, :street_address, :string
    add_column :retailers, :post_code, :string
    add_column :retailers, :post_area, :string
    add_column :retailers, :website, :string
  end
end
