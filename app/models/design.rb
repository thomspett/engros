class Design < ApplicationRecord
	has_and_belongs_to_many :yarns

	has_one_attached :main_image
	has_one_attached :pattern

	scope :by_yarn, -> (yarn) { joins(:yarns).where(yarns: {name: yarn}) }
end
