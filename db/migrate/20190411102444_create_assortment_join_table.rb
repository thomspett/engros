class CreateAssortmentJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :retailers, :yarns do |t|
      t.index [:retailer_id, :yarn_id]
      t.index [:yarn_id, :retailer_id], unique: true
    end
  end
end
