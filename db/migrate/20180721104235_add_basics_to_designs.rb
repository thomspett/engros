class AddBasicsToDesigns < ActiveRecord::Migration[5.2]
  def change
    add_column :designs, :name, :string
    add_column :designs, :description, :text
  end
end
