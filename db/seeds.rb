# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Rails.env.development?
	Admin.create(
		email: 'admin@example.com',
		password: '1234qwer',
	)

	User.create(
	  email: 'test@example.com',
	  password: '1234qwer',
	)

	coton3 = Yarn.create(
		name: 'Coton 3',
		length: 121,
		weight: 50,
	)

	multico = Yarn.create(
		name: 'Multico',
		length: 143,
		weight: 50,
	)

	falaise = Yarn.create(
		name: 'Falaise',
		length: 10000,
		weight: 50,
	)

	rustique = Yarn.create(
		name: 'Rustique',
		length: 10000,
		weight: 50,
	)

	alban = Design.create(
		name: 'Alban genser 4-12 år',
		description: 'Fargerikt garn trenger ikke mye ekstra, men denne barske genseren har fått skulderknepping og falsk lommeklaff.',
		yarns: [multico, coton3],
	)

	alban.main_image.attach(
		io: File.open('app/assets/seeds/images/684-16-alban-philmultico-philcoton3-phildar.jpg'),
		filename: '684-16-alban-philmultico-philcoton3-phildar.jpg',
		content_type: 'image/jpg',
	)

	eva = Design.create(
		name: 'Eva jakke XS-XXXL',
		description: 'Delikat stripejakke med fint avstemt garn- og fargemiks. Alt i rillestrikk og raglanfelt bærestykke.',
		yarns: [coton3, falaise, rustique]
	)

	eva.main_image.attach(
		io: File.open('app/assets/seeds/images/153-10-falaise-coton3-brillant-phildar.jpg'),
		filename: '153-10-falaise-coton3-brillant-phildar.jpg',
		content_type: 'image/jpg',
	)

	design1 = Design.create(
	  name: "Patentjakke med fletter",
	  description: "Røff helpatent møter elegante, smale fletter i denne velkomponerte jakken med kinainspirert hals og lukking.",
	)
	design1.main_image.attach(
	  io: File.open('app/assets/seeds/images/110011_patentjakke_phildar.jpg'),
	  filename: '110011_patentjakke_phildar.jpg',
	  content_type: 'image/jpg',
	)
	design1.pattern.attach(
	  io: File.open('app/assets/seeds/documents/2110-PHIL-655070-MIKS14 655-7.pdf'),
	  filename: '2110-PHIL-655070-MIKS14 655-7.pdf',
	  #content_type: 'document/pdf',
	)

	butikk1 = Retailer.create(
	  name: "Toffebutikken",
	  yarns: [multico, coton3],
	)

	butikk2 = Retailer.create(
	  name: "Garnbutikken",
	  yarns: [rustique, falaise, multico, coton3],
	)

end
