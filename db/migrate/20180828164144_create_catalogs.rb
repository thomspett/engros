class CreateCatalogs < ActiveRecord::Migration[5.2]
  def change
    create_table :catalogs do |t|
      t.string :title
      t.datetime :published_at

      t.timestamps
    end
  end
end
