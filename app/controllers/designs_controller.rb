class DesignsController < ApplicationController
  before_action :authenticate_admin!, except: [ :show, :index ]

  def index
		@designs = Design.with_attached_main_image.where(nil)
    @designs = @designs.by_yarn(params[:yarn]) if params[:yarn].present?
	end

	def show
		@design = Design.find(params[:id])
	end

  def new
  	@design = Design.new
  end

  def create
  	@design = Design.new(design_params)

  	if @design.save
  		redirect_to @design
  	else
  		render 'new'
  	end
  end

  def destroy
		@design = Design.find(params[:id])
		@design.main_image.purge
		@design.destroy

		redirect_to designs_path
	end

  private
		def design_params
			params.require(:design).permit(:main_image, :name, :description)
		end

end
