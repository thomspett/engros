class AddContactToLead < ActiveRecord::Migration[5.2]
  def change
    add_column :leads, :contact, :string
  end
end
