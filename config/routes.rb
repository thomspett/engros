Rails.application.routes.draw do

  devise_for :users
  devise_for :admins

  namespace :manager do
    resources :yarns
    resources :admins
    resources :users
    resources :designs
    resources :retailers
    resources :leads
    resources :catalogs

    root to: "admins#index"
  end

  resources :main

	resources :retailers
	resources :designs
  resources :leads
  resources :catalogs
  resources :yarns

	root 'main#leads'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
