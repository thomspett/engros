class RetailersController < ApplicationController
	before_action :authenticate_admin!, except: [ :show, :index ]


	def index
		@retailers = Retailer.order(:name)
	end

	def show
		@retailer = Retailer.find(params[:id])
	end

	def new
		@retailer = Retailer.new
	end

	def edit
		@retailer = Retailer.find(params[:id])
	end

	def update
		@retailer = Retailer.find(params[:id])

		if @retailer.update(retailer_params)
			redirect_to @retailer
		else
			render 'edit'
		end
	end

	def create
		@retailer = Retailer.new(retailer_params)

		if @retailer.save
			redirect_to @retailer
		else
			render 'new'
		end
	end

	def destroy
		@retailer = Retailer.find(params[:id])
		@retailer.destroy

		redirect_to retailers_path
	end

	private
		def retailer_params
			params.require(:retailer).permit(:description, :name, :website, :street_address, :post_code, :post_area)
		end
end