class LeadsController < ApplicationController
	def create
		@lead = Lead.new(lead_params)

		if @lead.save
			flash[:notice] = "Du er påmeldt!"
			redirect_to root_path
		else
			render 'main/leads'
		end
	end

  private
  	def lead_params
  		params.require(:lead).permit(:email, :contact, :company_name)
  	end

end
